package br.com.javanei.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "produto")
@NamedQueries({
        @NamedQuery(name = "TipoProduto.findByCodigo", query = "SELECT p FROM TipoProduto p WHERE p.codigo = :codigo"),
        @NamedQuery(name = "TipoProduto.findAll", query = "SELECT p FROM TipoProduto p")})
public class TipoProduto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "codigo", nullable = false, length = 40, unique = true)
    private String codigo;
    @Column(name = "descricao", nullable = false, length = 240)
    private String descricao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof TipoProduto)) {
            return false;
        }
        TipoProduto other = (TipoProduto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TipoProduto{" + "id=" + id + ", codigo=" + codigo + ", descricao=" + descricao + '}';
    }
}
