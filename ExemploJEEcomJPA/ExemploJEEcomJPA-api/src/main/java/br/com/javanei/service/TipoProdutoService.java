package br.com.javanei.service;

import br.com.javanei.entity.TipoProduto;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface TipoProdutoService {
    public TipoProduto create(String codigo, String descricao) throws Exception;

    public List<TipoProduto> findAll();

    public TipoProduto findByCodigo(String codigo);
}
