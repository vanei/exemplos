/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.javanei.rest;

import br.com.javanei.entity.TipoProduto;
import br.com.javanei.service.TipoProdutoService;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("tipoproduto")
public class TipoProdutoRest {

    @EJB
    private TipoProdutoService tipoService;

    @GET
    @Produces("application/json")
    public List<TipoProduto> findAll() {
        return tipoService.findAll();
    }

    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public TipoProduto create(String codigo, String descricao) throws Exception {
        return tipoService.create(codigo, descricao);
    }
}
