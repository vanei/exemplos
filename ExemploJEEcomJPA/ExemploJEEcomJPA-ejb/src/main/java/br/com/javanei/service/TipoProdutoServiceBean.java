package br.com.javanei.service;

import br.com.javanei.entity.TipoProduto;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
public class TipoProdutoServiceBean implements TipoProdutoService {
    @PersistenceContext(unitName = "ExemploPU")
    private EntityManager em;

    @Override
    public TipoProduto create(String codigo, String descricao) throws Exception {
        TipoProduto t = new TipoProduto();
        t.setCodigo(codigo);
        t.setDescricao(descricao);

        em.persist(t);

        return t;
    }

    @Override
    public List<TipoProduto> findAll() {
        TypedQuery<TipoProduto> q = (TypedQuery<TipoProduto>) em.createNamedQuery("TipoProduto.findAll");
        return q.getResultList();
    }

    @Override
    public TipoProduto findByCodigo(String codigo) {
        TypedQuery<TipoProduto> q = (TypedQuery<TipoProduto>) em.createNamedQuery("TipoProduto.findByCodigo");
        q.setParameter("codigo", codigo);
        return q.getSingleResult();
    }
}
